from Currency import Currency
from DispenseChain.Chains import TwoHundredBOBDispenser, OneHundredBOBDispenser, FiftyBOBDispenser, \
    TwentyBOBDispenser, TenBOBDispenser


class AtmMachine:
    def __init__(self):
        two_hundred_bob_dispenser = TwoHundredBOBDispenser()
        one_hundred_bob_dispenser = OneHundredBOBDispenser()
        fifty_bob_dispenser = FiftyBOBDispenser()
        twenty_bob_dispenser = TwentyBOBDispenser()
        ten_bob_dispenser = TenBOBDispenser()
        two_hundred_bob_dispenser.assign_next_chain(one_hundred_bob_dispenser)
        one_hundred_bob_dispenser.assign_next_chain(fifty_bob_dispenser)
        fifty_bob_dispenser.assign_next_chain(twenty_bob_dispenser)
        twenty_bob_dispenser.assign_next_chain(ten_bob_dispenser)
        self.chain = two_hundred_bob_dispenser

    def dispense(self, amount):
        if amount % 10 != 0:
            print("Ups, Amount should be in multiple of 10.")
        else:
            self.chain.dispense(Currency(amount))


if __name__ == '__main__':
    amount_requested = input("Please enter the amount requested: ")
    machine = AtmMachine()
    machine.dispense(int(amount_requested))
