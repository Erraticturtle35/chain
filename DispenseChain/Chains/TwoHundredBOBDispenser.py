from Currency.Currency import Currency
from DispenseChain.DispenseChain import DispenseChain


class TwoHundredBOBDispenser(DispenseChain):
    def dispense(self, currency):
        if currency.amount >= 200:
            amount_qty = currency.amount / 200
            remaining_amount = currency.amount % 200
            print("Dispensing:", int(amount_qty), "200 Bs.")
            if remaining_amount != 0:
                self.chain.dispense(Currency(remaining_amount))
        else:
            self.chain.dispense(currency)

    def assign_next_chain(self, chain):
        self.chain = chain
