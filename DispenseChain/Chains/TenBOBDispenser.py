from Currency.Currency import Currency
from DispenseChain.DispenseChain import DispenseChain


class TenBOBDispenser(DispenseChain):
    def dispense(self, currency):
        if currency.amount >= 10:
            amount_qty = currency.amount / 10
            remaining_amount = currency.amount % 10
            print("Dispensing:", int(amount_qty), "10 Bs.")
            if remaining_amount != 0:
                self.chain.dispense(Currency(remaining_amount))
        else:
            self.chain.dispense(currency)

    def assign_next_chain(self, chain):
        self.chain = chain
