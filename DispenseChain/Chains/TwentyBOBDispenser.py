from Currency.Currency import Currency
from DispenseChain.DispenseChain import DispenseChain


class TwentyBOBDispenser(DispenseChain):
    def dispense(self, currency):
        if currency.amount >= 20:
            amount_qty = currency.amount / 20
            remaining_amount = currency.amount % 20
            print("Dispensing:", int(amount_qty), "20 Bs.")
            if remaining_amount != 0:
                self.chain.dispense(Currency(remaining_amount))
        else:
            self.chain.dispense(currency)

    def assign_next_chain(self, chain):
        self.chain = chain
