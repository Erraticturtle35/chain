from Currency.Currency import Currency
from DispenseChain.DispenseChain import DispenseChain


class OneHundredBOBDispenser(DispenseChain):
    def dispense(self, currency):
        if currency.amount >= 100:
            amount_qty = currency.amount / 100
            remaining_amount = currency.amount % 100
            print("Dispensing:", int(amount_qty), "100 Bs.")
            if remaining_amount != 0:
                self.chain.dispense(Currency(remaining_amount))
        else:
            self.chain.dispense(currency)

    def assign_next_chain(self, chain):
        self.chain = chain
