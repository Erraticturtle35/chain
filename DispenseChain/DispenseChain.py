from abc import ABC, abstractmethod


class DispenseChain(ABC):
    def __init__(self):
        self.chain = None

    @abstractmethod
    def assign_next_chain(self, chain):
        pass

    @abstractmethod
    def dispense(self, currency):
        pass
